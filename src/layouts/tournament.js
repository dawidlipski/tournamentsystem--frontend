import React, { Component } from 'react';
import './styles/tournament.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tournamentActions from '../actions/tournamentActions';
import { Bracket, BracketGame, BracketGenerator, Model} from 'react-tournament-bracket';
import _ from 'lodash';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import '../constLayout/menu.css'
import '../constLayout/scrollbar.css';
import '../constLayout/layout.css';
import Menu from '../constLayout/menu.js';
import SwipeableTemporaryDrawer from '../constLayout/SwipeableTemporaryDrawer.js';
import {Growl} from 'primereact/growl';
import {Accordion,AccordionTab} from 'primereact/accordion';
import ProgressBar from './progressBar';

class Tournament extends Component {

  constructor(props){
    super(props);
    this.levels = [];
    this.state = {
      homeOnTopState: true,
      hoveredTeamId: null,
    }
  }

  componentDidMount(){
    this.props.actions.fetchUsers().then( () => {
      this.props.actions.fetchTeams().then( () => {
        this.props.actions.fetchTournamentById(this.props.match.params.id);
      });
    });
  }

  convertTournament(matches, date){
    let convertedTournament = this.mapOneMatch(matches[0], 0, date, matches);
    return convertedTournament
  }

  convertMatchesToArray(data){
    let height = this.props.tournament.height;
    let convertedArray = [];

    for(let k = 0; k < height; k++){
      convertedArray[k] = [];
    }

    for(let i = 0; i < height; i++){
      for(let j = 0; j < data.length; j++){
        if(data[j].whichRound == i){
          convertedArray[i].push(data[j]);
        }
      }
    }
    return convertedArray;
  }


  arrayToRoundsFFA(games, date){
    if(games == null){
      return null;
    }

    return(
        games.map( (round) => {
          return (
            <div className="ffa-round">
              <span><b>Round {round[0].whichRound + 1}</b></span>
              {this.roundToMatchesFFA(round, date)}
            </div>
          )
        })
    )
  }

  roundToMatchesFFA(round, date){
    if(round == null){
      return null
    }

    return(
      <div>
        {round.map( (match) => {
          return (
            <div className="ffa-game">
              {this.mapFFAMatch(match, date)}
            </div>
          )
        })}
      </div>
    )

  }

  mapFFAMatch(game, date){
    const { homeOnTopState } = this.state;
    const { gameComponent: GameComponent } = this;

    if(game == null){
      return null;
    }

    let match = {
      "id": game.gameId,
      "externalId": game.id,
      "scheduled": Number(date),
      "sides": {
        "home": {
          "team": {
            "id": game.idOfFirstPlayer,
            "name": this.mapIdToUserNames(game.idOfFirstPlayer)
          },
          "score":this.returnHomeScore(game.idWinner, game.idOfFirstPlayer)
        },
        "visitor": {
          "team": {
            "id": game.idOfSecondPlayer,
            "name": this.mapIdToUserNames(game.idOfSecondPlayer)
          },
          "score":this.returnVisitorScore(game.idWinner, game.idOfSecondPlayer)
        }
      }
    }

    return(
      <Bracket GameComponent={GameComponent} game={match}></Bracket>
    )
  }

  mapOneMatch(game, round, date, matches){
    if(game == null || game.gameId == null){
      return null;
    }

    let leftNode = matches[game.gameId * 2 + 1];
    let rightNode = matches[game.gameId * 2 + 2];
    return{
      "id": game.gameId,
      "name": this.resolveTournamentPhase(round),
      "externalId": game.id,
      "scheduled": Number(date),
      "sides": {
        "home": {
          "team": {
            "id": game.idOfFirstPlayer,
            "name": this.mapIdToUserNames(game.idOfFirstPlayer)
          },
          "score": this.returnHomeScore(game.idWinner, game.idOfFirstPlayer),
          "seed": {
            "displayName": "",
            "rank": 1,
            "sourceGame": this.mapOneMatch(leftNode, round + 1, date, matches)
          }
        },
        "visitor": {
          "team": {
            "id": game.idOfSecondPlayer,
            "name": this.mapIdToUserNames(game.idOfSecondPlayer)
          },
          "score": this.returnVisitorScore(game.idWinner, game.idOfSecondPlayer),
          "seed": {
            "displayName": "",
            "rank": 1,
            "sourceGame": this.mapOneMatch(rightNode, round + 1, date, matches)
          }
        }
      }

    }

  }

  returnHomeScore(idWinner, idOfHomePlayer){
    if(idWinner == null){
      return{
        "score": "0"
      }
    } else {
      return{
        "score": idWinner === idOfHomePlayer ? 1 : 0
      }
    }
  }

  returnVisitorScore(idWinner, idOfVisitorPlayer){
    if(idWinner == null){
      return{
        "score": "0"
      }
    } else {
      return{
        "score": idWinner === idOfVisitorPlayer ? 1 : 0
      }
    }
  }

  resolveTournamentPhase(round) {
    let basePhases = [
      "Final",
      "Semi-final",
      "Quarter-final"
    ];
    if (round >= 0 && round < 3) {
      return basePhases[round];
    } else if (round >= 3) {
      return `Round of ${2 ** (round + 1)}`;
    } else {
      return "Unknow";
    }
  }

  mapIdToUserNames(id){
    let users = this.props.userList;
    let teams = this.props.teamList;
    let name;

    if(!users || !teams)
    return;


    for(let i = 0; i < users.length; i++){
      if(users[i].id === id){
        name = users[i].nick;
      }
    }

    if(name === undefined){
      for(let i = 0; i < teams.length; i++){
        if(teams[i].id === id){
          name = teams[i].group_name;
        }
      }
    }

    return name;
  }

  getPlayerScore(games, id){
    let points = 0;
    for(let i = 0; i < games.length; i++){
      if(games[i].idWinner == id){
        points++;
      }
    }

    return points;
  }

  render() {
    const { homeOnTopState } = this.state;
    const { gameComponent: GameComponent } = this;

    if(!this.props.tournament){
      return (
        <ProgressBar />
      )
    } else if(this.props.tournament.tournamentType == "BRACKET"){
      if(!this.props.tournament.games)
        return null;

      return(
        <div className="p-g p-g-nopad">
          <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
          <Menu />
          </div>
          <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
            <div className="tournament-container">
                <div className="tournament-label">
                   <button className="button" onClick={() => {this.props.history.replace("/show")}}> &lt;- Back</button>Tournament Name: {this.props.tournament.tournamentName}
                </div>
                <div className="tournament-bracket">
                  <Growl ref={(el) => this.growl = el} />
                  <Bracket GameComponent={GameComponent} game={this.convertTournament(this.props.tournament.games, this.props.tournament.date)}/>
                </div>
            </div>
          </div>
          <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{height: "70px"}}></div>
        </div>
      )
    } else if(this.props.tournament.tournamentType == "FFA"){
      if(!this.props.tournament.games){
        return null;
      }
      let tournamentPlayers = [];
      let uniqTournamentPlayers = [];
      let statistics = [];
      let games = this.props.tournament.games;
      for(let i = 0; i < this.props.tournament.games.length; i++){
        tournamentPlayers.push(this.props.tournament.games[i].idOfFirstPlayer);
        tournamentPlayers.push(this.props.tournament.games[i].idOfSecondPlayer);
      }
      uniqTournamentPlayers = [... new Set(tournamentPlayers) ]

      for(let j = 0; j < uniqTournamentPlayers.length; j++){
        statistics[j] = [];
        statistics[j].push(this.mapIdToUserNames(uniqTournamentPlayers[j]));
        statistics[j].push(this.getPlayerScore(games, uniqTournamentPlayers[j]));
      }

      statistics.sort((a,b) => parseInt(b[1]) - parseInt(a[1]));
      return (
        <div className="p-g p-g-nopad">
          <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
          <Menu />
          </div>
          <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
            <div className="tournament-container">
              <div className="tournament-label">
                <button className="button" onClick={() => {this.props.history.replace("/show")}}> &lt;- Back</button><b>Tournament Name:</b> {this.props.tournament.tournamentName}
              </div>
              <div className="tournament-ffa">
                <div className="tournament-statistics">
                  <Accordion>
                    <AccordionTab header="Statistics">
                      <div className="tournament-stat-labels">
                        <div className="tournament-stat-lp" style={{borderTop: '1px solid black'}}> Lp. </div>
                        <div className="tournament-stat-nick" style={{borderTop: '1px solid black'}}> <b>Nick</b></div>
                        <div className="tournament-stat-score" style={{borderTop: '1px solid black'}}> <b>Score</b></div>
                      </div>
                      {statistics.map( (stat, index) => {
                        return(
                          <div className="tournament-stat">
                            <div className="tournament-stat-lp"> {index + 1}.</div>
                            <div className="tournament-stat-nick"> {stat[0]}</div>
                            <div className="tournament-stat-score"> {stat[1]}</div>
                          </div>
                        )
                      })}
                    </AccordionTab>
                  </Accordion>
                </div>
                <Growl ref={(el) => this.growl = el} />
                {this.arrayToRoundsFFA(this.convertMatchesToArray(this.props.tournament.games), this.props.tournament.date)}
              </div>
            </div>
          </div>
          <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{height: "70px"}}></div>
        </div>
      )
    }
  }

  handleClick = (game: Model.game) => {
    if(this.props.tournament.gameType === "TURN_BASED_GAME"){
      this.props.actions.fetchMatch(game.externalId).then( () => {
        let game = this.props.game;
        if(game.idOfFirstPlayer == 0 || game.idOfFirstPlayer == null || game.idOfSecondPlayer == 0 || game.idOfSecondPlayer == null){
          this.growl.show({severity: 'error', summary: 'Something went wrong', detail: 'Match is not filled with participants yet!'});
        } else {
          if(game.idWinner == 0 || game.idWinner == null){
            this.props.actions.createBotMatch(game);
            this.growl.show({severity: 'success', summary: "Success", detail: 'Request to the Turn Base Game Server has been sent!'});
          } else {
            this.growl.show({severity: 'info', summary: 'Match', detail: 'Match is already finished!'});
            game.idStatistics == 0 ? null : this.growl.show({severity: 'info', summary: 'Match Statistics', detail: 'Id of match Statistics: ' + game.idStatistics}) 
          }
        }
      })
    } else {
        this.props.history.replace(`/match/${game.externalId}`)
      }
  }

  changeHoveredTeamId = (hoveredTeamId: string) => this.setState({ hoveredTeamId });

  gameComponent = (props) => {
    return (
      <BracketGame
      {...props}
      onHoveredTeamIdChange={this.changeHoveredTeamId}
      onClick={this.handleClick.bind(this, props.game)}
      hoveredTeamId={this.state.hoveredTeamId}
      />
    )
  };

}


const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(tournamentActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    tournament: state.tournament.tournament,
    game: state.tournament.game,
    userList: state.tournament.userList,
    teamList: state.tournament.teamList
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tournament);
