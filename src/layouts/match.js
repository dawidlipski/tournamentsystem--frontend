import React, { Component } from 'react';
import './styles/match.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tournamentActions from '../actions/tournamentActions';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import '../constLayout/menu.css'
import '../constLayout/scrollbar.css';
import '../constLayout/layout.css';
import Menu from '../constLayout/menu.js';
import { Button } from 'primereact/button';
import { Growl } from 'primereact/growl';
import { OverlayPanel } from 'primereact/overlaypanel';
import {Tooltip} from 'primereact/tooltip';
import ProgressBar from './progressBar';

class match extends Component {

  constructor(props) {
    super(props);
    this.state = {
      usersFirstTeam: [],
      usersSecondTeam: []
    }
  }

  componentDidMount() {
    this.props.actions.fetchMatch(this.props.match.params.id).then(() => {
      this.props.actions.fetchUsers()
      this.props.actions.fetchTeams()
        .then(() => this.fetchUsers());
        this.growl.show({life: '4000', severity: 'info', summary: 'Tip', detail: 'Click on team name to show users from team'})
    });
  }

  fetchUsers() {
    let idOfFirstPlayer = this.props.game.idOfFirstPlayer;
    let idOfSecondPlayer = this.props.game.idOfSecondPlayer;

    this.props.actions.fetchUsersFromTeam(idOfFirstPlayer)
      .then(() => {
        this.setState({ usersFirstTeam: this.props.teamUsersList });
      })

    this.props.actions.fetchUsersFromTeam(idOfSecondPlayer)
      .then(() => {
        this.setState({ usersSecondTeam: this.props.teamUsersList });
      })
  }

  mapIdToUserNames(id) {
    let users = this.props.userList;
    let teams = this.props.teamList;
    let name;

    if (!users || !teams)
      return;

    for (let i = 0; i < users.length; i++) {
      if (users[i].id === id) {
        name = users[i].nick;
      }
    }

    if (name === undefined) {
      for (let i = 0; i < teams.length; i++) {
        if (teams[i].id === id) {
          name = teams[i].group_name;
        }
      }
    }

    return name;
  }

  handleClick(id) {
    if (this.props.game.idOfFirstPlayer === null || this.props.game.idOfSecondPlayer === null) {
      this.growl.show({ severity: 'error', summary: 'Something went wrong', detail: 'One of sides is missing!' });
    } else if (this.props.game.idWinner) {
      this.growl.show({ severity: 'error', summary: 'Something went wrong', detail: 'Match already finished!' });
    } else {
      let game = {
        "idWinner": id,
        "idGame": this.props.match.params.id,
        "idStatistics": 0
      }

      this.props.actions.updateMatch(game).then(() => {
        this.growl.show({ severity: 'success', summary: "Match", detail: 'Succesful update!' });
        setTimeout(() => this.props.history.replace(`/tournament/${this.props.game.tournamentId}`), 1000)
      }).catch(() => {
        this.growl.show({ severity: 'error', summary: "Something went wrong", detail: 'Match is propably finished' });
        setTimeout(() => this.props.history.replace(`/tournament/${this.props.game.tournamentId}`), 1000)
      })
    }
  }

  renderWinButtonFirstPlayer(game) {
    if (game.gameType != 1) {
      return (
        <Button label="Set a Winner" className="p-button-warning-match" onClick={() => { this.handleClick(game.idOfFirstPlayer) }} />
      )
    }
  }

  renderWinButtonSecondPlayer(game) {
    if (game.gameType != 1) {
      return (
        <Button label="Set a Winner" className="p-button-warning-match" onClick={() => { this.handleClick(game.idOfSecondPlayer) }} />
      )
    }
  }

  renderUser(user) {
    let game = this.props.game;

    if (user === 0 && game.idOfFirstPlayer !== null) {
      return (
        <div>
          <div className="name" onClick={(e) => this.op1.toggle(e)}>
            {this.mapIdToUserNames(game.idOfFirstPlayer)}
          </div>
          <OverlayPanel ref={(el) => this.op1 = el}>
            <div>
              {this.state.usersFirstTeam.map((user, index) => {
                return (
                  <div>{user.nick}</div>
                )
              })}
            </div>
          </OverlayPanel>
        </div>
      )
    } else if (user === 1 && game.idOfSecondPlayer !== null) {
      return (
        <div>
          <div className="name" onClick={(e) => this.op2.toggle(e)}>
            {this.mapIdToUserNames(game.idOfSecondPlayer)}
          </div>
          <OverlayPanel ref={(el) => this.op2 = el}>
            <div>
              {this.state.usersSecondTeam.map((user, index) => {
                return (
                  <div>{user.nick}</div>
                )
              })}
            </div>
          </OverlayPanel>
        </div>
      )
    } else {
      return (
        <div>
          This side is not ready
        </div>
      )
    }
  }

  renderWinner() {
    if (!this.props.game.idWinner) {
      return null;
    } else {
      return (
        <div>
          Match Finished<br/>
          <span>{this.props.game.idStatistics == 0 ? null : "Id Statistics: " + this.props.game.idStatistics }</span>
        </div>
      )
    }
  }

  render() {
    let game = this.props.game;
    if (!game) {
      return null;
    }  else {
      return (
        <div className="p-g p-g-nopad">
          <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
            <Menu />
          </div>
          <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
            <div className="match-container">
              <Growl ref={(el) => this.growl = el} />

              <div className="match-left match">
                <div className="match-player">
                  {this.renderUser(0)}
                </div>
                <div className="match-winner-button">
                  {game.idOfFirstPlayer ? this.renderWinButtonFirstPlayer(game) : null}
                </div>
              </div>

              <div className="match-middle match">
                <div className="match-won-label">{this.renderWinner()}</div>
                <div className="match-vs">VS</div>
                <div className="match-back-button">
                  <Button label="Return" className="p-button-secondary" onClick={() => { this.props.history.replace(`/tournament/${this.props.game.tournamentId}`) }} />
                </div>
              </div>

              <div className="match-right match">
                <div className="match-player">{this.renderUser(1)}</div>
                <div className="match-winner-button">
                  {game.idOfSecondPlayer ? this.renderWinButtonSecondPlayer(game) : null}
                </div>
              </div>

            </div>
          </div>
          <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{ height: "70px" }}></div>
        </div>
      )
    }
  }

}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(tournamentActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    game: state.tournament.game,
    userList: state.tournament.userList,
    teamList: state.tournament.teamList,
    teamUsersList: state.tournament.teamUsersList
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(match);
