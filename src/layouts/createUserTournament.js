import React, { Component } from 'react';
import { PickList } from 'primereact/picklist';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tournamentActions from '../actions/tournamentActions';
import './styles/createMenu.css';
import Menu from '../constLayout/menu';
import { Growl } from 'primereact/growl';
import { SelectButton } from 'primereact/selectbutton';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { TabView, TabPanel } from 'primereact/tabview';
import { InputTextarea } from 'primereact/inputtextarea';

const selectTournamentType = [
    { label: 'Bracket', value: 0 },
    { label: 'League', value: 1 },
];

const selectGameType = [
    { label: 'Foosball', value: 0 },
    { label: 'Custom', value: 2 },
];

class CreateUserTournament extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            selectedGame: 0,
            selectedType: 0,
            ifSolo: true,
            userSource: [],
            userTarget: [],
            description: '',
            isButtonDisabled: false
        };
    }

    componentDidMount() {
        this.props.actions.fetchUsers()
            .then(() => { this.setState({ userSource: this.props.userList }) });
    }

    tournamentCreated() {
        this.growl.show({ severity: 'success', summary: 'Success', detail: 'Tournament created' })
        setTimeout(() => this.props.history.replace("/show"), 1000)
    }

    handleClick() {

        this.setState({
            isButtonDisabled: true
          });

        let players = this.state.userTarget;
        let tournament = {
            "tournamentName": this.state.name,
            "tournamentType": this.state.selectedType,
            "gameType": this.state.selectedGame,
            "ifSolo": this.state.ifSolo,
            "players": players,
            "description": this.state.description
        }

        if (players.length < 2) {
            this.growl.show({ severity: 'error', summary: '', detail: 'Number of participants must be higher than 1' });
        } else if (this.state.name == '') {
            this.growl.show({ severity: 'error', summary: '', detail: 'Enter name of tournament!' });
        } else if (this.state.name.length > 25) {
            this.growl.show({ severity: 'error', summary: '', detail: 'Name can have maximum 25 characters' });
        } else if (this.state.description.length > 250) {
            this.growl.show({ severity: 'error', summary: '', detail: 'Description can have maximum 250 characters' });
        } else {
            this.props.actions.createTournament(tournament)
                .then(() => this.tournamentCreated())
                .catch(() => {
                    this.growl.show({ severity: 'error', summary: 'Something went wrong', detail: 'Error during creation of tournament!' })
                    this.setState({isButtonDisabled: false});
                })
        }
    }

    renderSelects() {

        if (this.state.selectedType == null) {
            this.setState({selectedType: 0});
        }

        if (this.state.selectedGame == null) {
            this.setState({selectedGame: 0});
        }

        return (
            <div>
                <div className="form-row" style={{ marginLeft: '4em' }}>
                    <p className="form-text form-text-name">Tournament name</p>
                    <div className="p-g p-fluid">
                        <div className="p-inputgroup div-input">
                            <InputText keyfilter={/^[^#<>*'!\s]+$/} placeholder="Enter name of a tournament"
                                className="input-text"
                                onChange={(event) => this.setState({ name: event.target.value })}
                                value={this.state.name} />
                        </div>
                    </div>
                </div>
                <div className="form-row" style={{ marginLeft: '4em' }}>
                    <p className="form-text form-text-name" style={{ marginBottom: '.5em' }}>Tournament description</p>
                    <div className="p-g p-fluid">
                        <div className="p-inputgroup div-input">
                            <InputTextarea rows={5} cols={70} value={this.state.description} placeholder="Enter tournament description"
                                onChange={(e) => this.setState({ description: e.target.value })} autoResize={true} />
                        </div>
                    </div>
                </div>
                <div className="form-row" style={{ marginLeft: '0px' }}>
                    <p className="form-text">Select tournament type</p>
                    <SelectButton value={this.state.selectedType} options={selectTournamentType} onChange={(e) => this.setState({ selectedType: e.value })} 
                        tooltip="League tournament can have max 32 players" tooltipOptions={{position: 'top'}}></SelectButton>
                </div>
                <div className="form-row" style={{ marginLeft: '0px' }}>
                    <p className="form-text">Select game type</p>
                    <SelectButton value={this.state.selectedGame} options={selectGameType} onChange={(e) => this.setState({ selectedGame: e.value })}></SelectButton>
                </div>
            </div >
        )
    }

    userTemplate(userList) {
        return (
            <div>
                {userList.nick}
            </div>
        )
    }

    onTabChange(e) {
        if (e.index === 1) {
            this.props.history.replace(`/create/team`);
        } else if (e.index === 2) {
            this.props.history.replace(`/create/bot`);
        }
    }

    render() {
        return (
            <div className="p-g p-g-nopad">
                <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad render-menu height1">
                    <Menu />
                </div>
                <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad content height2">
                    <Growl ref={(el) => this.growl = el}></Growl>
                    <TabView activeIndex={0} onTabChange={(e) => this.onTabChange(e)}>
                        <TabPanel header="User">
                            <div className="my-form">
                                {this.renderSelects()}
                                <div className="picklist-users">
                                    <PickList
                                        source={this.state.userSource}
                                        target={this.state.userTarget}
                                        itemTemplate={this.userTemplate}
                                        sourceHeader="Available"
                                        targetHeader="Selected"
                                        onChange={(e) => this.setState({ userSource: e.source, userTarget: e.target })}
                                        responsive={true}
                                        showTargetControls={false}
                                        showSourceControls={false}
                                    />
                                </div>
                                <div className="button-create">
                                    <Button label="Create tournament" onClick={() => this.handleClick()}  disabled={this.state.isButtonDisabled}
                                        style={{ height: '4em', width: '15em', margin: '0 auto', backgroundColor: '#FF7F28', border: 'none' }} />
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel header="Team">
                        </TabPanel>
                        <TabPanel header="Bots">
                        </TabPanel>
                    </TabView>
                </div>
                <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{ height: "70px" }}></div>
            </div>
        )
    }


}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(tournamentActions, dispatch)
    }
}

const mapStateToProps = (state) => {
    return {
        userList: state.tournament.userList,
        tournament: state.tournament.tournament
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateUserTournament);
