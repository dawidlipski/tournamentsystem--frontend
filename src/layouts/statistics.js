import React, { Component } from 'react';
import './styles/statistics.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tournamentActions from '../actions/tournamentActions';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import '../constLayout/menu.css'
import '../constLayout/scrollbar.css';
import '../constLayout/layout.css';
import Menu from '../constLayout/menu.js';
import SwipeableTemporaryDrawer from '../constLayout/SwipeableTemporaryDrawer.js';
import { BrowserRouter, History, Link } from "react-router-dom";
import {Panel} from 'primereact/panel';
import {SelectButton} from 'primereact/selectbutton';
import ProgressBar from './progressBar';


class statistics extends Component {

constructor(props){
  super(props);
  this.state = {
    name: "",
    surname: "",
    nick: "",
    search: "",
    selectedType: "0",
    selectedGame: "0",
  };
}

componentDidMount(){
  this.props.actions.fetchUsers();
}

render(){
  if (!this.props.userList) {
    return (
      <ProgressBar />
    )
  } else {
    let filteredUsers = this.props.userList;
    filteredUsers = filteredUsers.filter(
      (user) => {return user.nick.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
      }
    )
    return(
      <div className="p-g p-g-nopad">
        <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
        <Menu />
        </div>
        <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
          <div className="statistics-container">
            <div className="statistics-search">
            <input type="text" placeholder="Search by nickname..." value={this.state.search} onChange={ (event) => this.setState({search: event.target.value})}></input><br/>
            </div>
            <div className="statistics-main">
              {filteredUsers.map( (user) => {
                const header = (
                  <div className="header">
                    <div className="header-left">{user.nick}</div>

                    <div className="header-right">
                      Tournaments Won: {user.statistics.tournamentWon} | Tournament Played: {user.statistics.tournamentPlayed}
                    </div>
                  </div>
                )
                const gameTypes = [
                  {label: 'Foosball', value: '0'},
                  {label: 'Turn Based Game', value: '1'},
                  {label: 'Custom', value: '2'}
                ];
                const tournamentTypes = [
                  {label: 'Bracket', value: '0'},
                  {label: 'FFA', value: '1'}
                ];
                let subTournamentPlayed = 0;
                let subTournamentWon = 0;
                for(let i = 0; i < user.statistics.subStatistics.length; i++){
                  if(this.state.selectedType == user.statistics.subStatistics[i].tournamentType && this.state.selectedGame == user.statistics.subStatistics[i].gameType){
                    subTournamentPlayed = user.statistics.subStatistics[i].tournamentPlayed;
                    subTournamentWon = user.statistics.subStatistics[i].tournamentWon;
                  }
                }
                return (
                  <Panel header={header} style={{marginBottom: '5px'}}>
                    <div className="content-left">
                      <SelectButton value={this.state.selectedGame} options={gameTypes} onChange={(e) => this.setState({selectedGame: e.value})}></SelectButton>
                    </div>
                    <div className="content-right">
                      <SelectButton value={this.state.selectedType} options={tournamentTypes} onChange={(e) => this.setState({selectedType: e.value})}></SelectButton>
                    </div>
                    <div className="content-bottom">
                      Tournaments Won: {subTournamentWon} | Tournaments Played: {subTournamentPlayed}
                    </div>
                  </Panel>
                )
              })}
            </div>
          </div>
        </div>
        <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{height: "70px"}}></div>
      </div>
    )
  }
}

}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(tournamentActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    userList: state.tournament.userList,
    teamList: state.tournament.teamList
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(statistics);
