import React, { Component } from 'react';
import './styles/showList.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as tournamentActions from '../actions/tournamentActions';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import '../constLayout/menu.css'
import '../constLayout/scrollbar.css';
import '../constLayout/layout.css';
import Menu from '../constLayout/menu.js';
import SwipeableTemporaryDrawer from '../constLayout/SwipeableTemporaryDrawer.js';
import { Accordion, AccordionTab } from 'primereact/accordion';
import { Button } from 'primereact/button';
import { Growl } from 'primereact/growl';
import { Checkbox } from 'primereact/checkbox';
import ProgressBar from './progressBar';

class showTournaments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      played: true,
      won: true,
      ffa: true,
    }
  }

  componentDidMount() {
    this.props.actions.fetchUsers().then(() => {
      this.props.actions.fetchTeams().then(() => {
        this.props.actions.fetchTournaments();
      });
    });
  }

deleteTournament(id, name) {
        this.props.actions.deleteTournament(id).then( () => {
          this.growl.show({severity: 'success', summary: name, detail: 'Tournament Deleted'});
          this.props.actions.fetchTournaments().then( () => {
          });
        })
        .catch( () => {
          this.growl.show({severity: 'error', summary: 'Something went wrong', detail: 'Could not delete tournament'});
        });
    }

  mapIdToUserNames(id) {
    let users = this.props.userList;
    let teams = this.props.teamList;
    let name;

    if (!users || !teams)
      return;


    for (let i = 0; i < users.length; i++) {
      if (users[i].id === id) {
        name = users[i].nick;
      }
    }

    if (name === undefined) {
      for (let i = 0; i < teams.length; i++) {
        if (teams[i].id === id) {
          name = teams[i].group_name;
        }
      }
    }

    return name;
  }

  getTournamentsPlayed() {
    let tournamentsPlayed = [];
    let tournaments = this.props.tournamentList;
    tournaments.map((tournament) => {
      if (tournament.winnerId == 0 && tournament.tournamentType != "FFA")
        tournamentsPlayed.push(tournament);
    })

    return tournamentsPlayed;
  }

  getTournamentsWon() {
    let tournamentsWon = [];
    let tournaments = this.props.tournamentList;
    tournaments.map((tournament) => {
      if (tournament.winnerId != 0 && tournament.tournamentType != "FFA")
        tournamentsWon.push(tournament);
    })

    return tournamentsWon;
  }

  getFFATournaments() {
    let FFATournaments = [];
    let tournaments = this.props.tournamentList;
    tournaments.map((tournament) => {
      if (tournament.tournamentType == "FFA")
        FFATournaments.push(tournament);
    })

    return FFATournaments;
  }

  getFilteredTournaments() {
    let tournamentsPlayed = this.getTournamentsPlayed();
    let tournamentsWon = this.getTournamentsWon();
    let FFATournaments = this.getFFATournaments();
    let tournamentsFiltered = [];

    if (this.state.played == true) {
      tournamentsFiltered = tournamentsFiltered.concat(tournamentsPlayed);
    }
    if (this.state.won == true) {
      tournamentsFiltered = tournamentsFiltered.concat(tournamentsWon);
    }
    if (this.state.ffa == true) {
      tournamentsFiltered = tournamentsFiltered.concat(FFATournaments);
    }

    return tournamentsFiltered;

  }

  renderTournaments() {
    if (!this.props.tournamentList) {
      return null;
    } else {
      let filteredTournaments = this.getFilteredTournaments();
      filteredTournaments = filteredTournaments.filter(
        (tournament) => { return tournament.tournamentName.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1 }
      )
      return (
        <div>
          <div className="search-bar">
            <input type="text" placeholder="Search by tournament name..." value={this.state.search} onChange={(event) => this.setState({ search: event.target.value })}></input><br />
            <Checkbox onChange={(event) => this.setState({ played: !this.state.played })} checked={this.state.played}>
            </Checkbox> Tournaments In Progress &emsp;
            <Checkbox onChange={(event) => this.setState({ won: !this.state.won })} checked={this.state.won}>
            </Checkbox> Tournaments Finished &emsp;
            <Checkbox onChange={(event) => this.setState({ ffa: !this.state.ffa })} checked={this.state.ffa}>
            </Checkbox> FFA
          </div>
          <Accordion>
            {filteredTournaments.map((tournament, index) => {
              let progress;
              if (tournament.winnerId === 0)
                progress = <span>Status: <b>In Progress</b></span>
              else if (tournament.winnerId !== 0 && tournament.tournamentType == "FFA") {
                progress = <span>Leader: <b>{this.mapIdToUserNames(tournament.winnerId)}</b></span>
              }
              else
                progress = <span>Status: <b>Finished ({this.mapIdToUserNames(tournament.winnerId)})</b></span>
              let date = new Date(Number(tournament.date));
              date = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
              return (
                <AccordionTab header={tournament.tournamentName + " - " + date}>
                  <Growl ref={(el) => this.growl = el} />
                  {progress}<br />
                  {tournament.description ? <span>Description: <b>{tournament.description}</b><br /></span> : null}
                  Number of players: <b>{tournament.numberOfPlayers}</b> <br />
                  Type of Tournament: <b>{tournament.tournamentType}</b><br />
                  Type of Game: <b>{tournament.gameType}</b><br />
                  <Button label="Delete Tournament" className="p-button-danger" onClick={() => {
                    this.deleteTournament(tournament.id, tournament.tournamentName)
                  }} />
                  <Button label="Show Tournament" className="p-button-warning" onClick={() => { this.props.history.replace(`/tournament/${tournament.id}`) }} />
                </AccordionTab>
              )
            })}
          </Accordion>
        </div>
      )
    }
  }

  render() {
    if (!this.props.tournamentList) {
      return (
        <ProgressBar />
      )
    } else {
      return (
        <div className="p-g p-g-nopad">
          <div className="p-lg-3 p-md-12 p-sm-12 p-g-nopad height1">
            <Menu />
          </div>
          <div className="p-lg-9 p-md-12 p-sm-12 p-g-nopad height2">
            <div className="list-container">
              {this.renderTournaments()}
            </div>
          </div>
          <div className="p-g-12 p-md-12 p-sm-12 p-g-nopad footer" style={{ height: "70px" }}></div>
        </div>
      )
    }
  }


}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(tournamentActions, dispatch)
  }
}

const mapStateToProps = (state) => {
  return {
    tournamentList: state.tournament.tournamentList,
    userList: state.tournament.userList,
    teamList: state.tournament.teamList
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(showTournaments);
