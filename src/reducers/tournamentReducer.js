export default (state = {userList: null, teamList: null, tournamentList: null, botList: null}, action) => {
  switch(action.type) {
    case "FETCH_USERS_FULFILLED":{
      return {
        ...state,
        userList: action.payload.data
      }
    }
    case "FETCH_TEAMS_FULFILLED":{
      return {
        ...state,
        teamList: action.payload.data
      }
    }
    case "FETCH_USERS_FROM_TEAM_FULFILLED":{
      return {
        ...state,
        teamUsersList: action.payload.data
      }
    }
    case "FETCH_BOTS_FULFILLED":{
      return {
        ...state,
        botList: action.payload.data
      }
    }
    case "CREATE_TOURNAMENT_FULFILLED":{
      return {
        ...state,
        tournament: action.payload.data
      }
    }
    case "DELETE_TOURNAMENT_FULFILLED":{
      return {
        ...state,
        t: action.payload.data
      }
    }
    case "FETCH_TOURNAMENTS_FULFILLED":{
      return {
        ...state,
        tournamentList: action.payload.data
      }
    }
    case "FETCH_TOURNAMENT_BY_ID_FULFILLED":{
      return {
        ...state,
        tournament: action.payload.data
      }
    }
    case "FETCH_MATCH_FULFILLED":{
      return {
        ...state,
        game: action.payload.data
      }
    }
    case "UPDATE_MATCH_FULFILLED":{
      return {
        ...state,
        g: action.payload.data
      }
    }
    case "CREATE_MATCH_FULFILLED":{
      return {
        ...state,
        botGame: action.payload.data
      }
    }
    default: return state;
  }
}
