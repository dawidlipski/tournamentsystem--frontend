import { combineReducers } from 'redux';

import tournamentReducer from './tournamentReducer';

const rootReducer = combineReducers({
  tournament: tournamentReducer
})

export default rootReducer;
