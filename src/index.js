import React from 'react';
import ReactDOM from 'react-dom';
import mainMenu from './layouts/mainMenu';
import registerServiceWorker from './registerServiceWorker';
import { HashRouter, Route, Switch} from 'react-router-dom';
import { createStore, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import ReduxThunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import reducers from './reducers';
import statistics from './layouts/statistics';
import CreateUserTournament from './layouts/createUserTournament';
import CreateTeamTournament from './layouts/createTeamTournament';
import CreateBotTournament from './layouts/createBotTournament';
import showTournaments from './layouts/showTournaments.js';
import Tournament from './layouts/tournament.js';
import match from './layouts/match.js';

const store = createStore(
  reducers,
  applyMiddleware(logger, ReduxThunk, promiseMiddleware())
)

ReactDOM.render(
    <Provider store={store}>
      <HashRouter basename="/tournament">
        <Switch>
          <Route path="/tournament/:id" component={Tournament}/>
          <Route path="/match/:id" component={match}/>
          <Route path="/create/user" component={CreateUserTournament}/>
          <Route path="/create/team" component={CreateTeamTournament}/>
          <Route path="/create/bot" component={CreateBotTournament}/>
          <Route path="/create" component={CreateUserTournament}/>
          <Route path="/show" component={showTournaments}/>
          <Route path="/statistics" component={statistics}/>
          <Route path="/" component={mainMenu}/>
        </Switch>
      </HashRouter>
    </Provider>
   ,document.getElementById('root'));
registerServiceWorker();
