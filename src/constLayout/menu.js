import React, { Component } from 'react';
import SwipeableTemporaryDrawer from './SwipeableTemporaryDrawer.js';
import { BrowserRouter, History, Link } from "react-router-dom"

class menu extends Component{

constructor(props){
  super(props);
}

render(){
  return (
    <div>
      <div className="burger">
      <SwipeableTemporaryDrawer />
      </div>

      <div className="menu">

        <ul>
          <li className="menu-item">
                  <div className="menu-item-icon">
                    <i className="pi pi-th-large" style={{'fontSize': '2em'}}></i>
                  </div>
                  <div className="menu-item-label">
                    <span className="span-label">Tournaments</span>
                    <ul>
                      <Link to="/create/user" className="link"><li><span>Create User Tournament</span></li></Link>
                      <Link to="/create/team" className="link"><li><span>Create Team Tournament</span></li></Link>
                      <Link to="/create/bot" className="link"><li><span>Create Bot Tournament</span></li></Link>
                    </ul>
                    </div>
                  <hr/>
          </li>
          <li className="menu-item">
                <div className="menu-item-icon">
                  <i className="pi pi-users" style={{'fontSize': '2em'}}></i>
                </div>
                <div className="menu-item-label">
                  <span className="span-label">Participants</span>
                  <ul>
                    <li onClick={() => window.location = `${process.env.REACT_APP_USRMGMT}/#/usrmgmt/create`}>Add User</li>
                    <li onClick={() => window.location = `${process.env.REACT_APP_USRMGMT}/#/usrmgmt/show`}>Manage Users</li>
                    <li onClick={() => window.location = `${process.env.REACT_APP_USRMGMT}/#/usrmgmt/createteam`}>Add Team</li>
                    <li onClick={() => window.location = `${process.env.REACT_APP_USRMGMT}/#/usrmgmt/showteams`}>Manage Teams</li>
                  </ul>
                  </div>
                <hr/>
          </li>
          <li className="menu-item">
              <div className="menu-item-icon">
                <i className="pi pi-globe" style={{'fontSize': '2em'}}></i>
              </div>
              <div className="menu-item-label">
                <Link to="/show" className="link">Games</Link>
                </div>
              <hr/>
          </li>
          <li className="menu-item">
          <div className="menu-item-icon">
            <i className="pi pi-info-circle" style={{'fontSize': '2em'}}></i>
          </div>
          <div className="menu-item-label">
            <Link to="/statistics" className="link">Statistics</Link>
            </div>
          <hr/>
          </li>
        </ul>

      </div>

    </div>
  )
}
}


export default menu;
