import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Data from './tileData';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import './drawer.css';

const styles = {
  list: {
    width: 270,
  },
  fullList: {
    width: 'auto',
  },
};

const theme = createMuiTheme({
  overrides: {
    MuiDrawer: { // Name of the component
      paper: { // Name of the rule
        background: 'rgba(51,51,51,1.0)', // Some CSS
      },
    },
  },
});

class SwipeableTemporaryDrawer extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    left: false,
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    const { classes } = this.props;

    const sideList = (
      <div className={classes.list}>
        <List><Data /></List>
      </div>
    );

    const fullList = (
      <div className={classes.fullList} >
        <List><Data /></List>
      </div>
    );

    return (
      <div>
        <div className="burger" onClick={this.toggleDrawer('left', true)}>
          <div className="burger-strip"></div>
          <div className="burger-strip"></div>
          <div className="burger-strip"></div>
        </div>
        <MuiThemeProvider theme={theme}>
          <SwipeableDrawer
            open={this.state.left}
            onClose={this.toggleDrawer('left', false)}
            onOpen={this.toggleDrawer('left', true)}
          >
            <div
              tabIndex={0}
              role="button"
              onClick={this.toggleDrawer('left', false)}
              onKeyDown={this.toggleDrawer('left', false)}
            >
              {sideList}
            </div>
          </SwipeableDrawer>
          </MuiThemeProvider>
      </div>
    );
  }
}

SwipeableTemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SwipeableTemporaryDrawer);
