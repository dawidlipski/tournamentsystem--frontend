// This file is shared across the demos.

import React from 'react';
import styled from 'styled-components';
import { BrowserRouter, History, Link } from "react-router-dom"

const Item = styled.div`
  color: #33ccff;
  font-size: 28px;
  margin-left: 10px;
  border-bottom: 1px solid white;
  margin-top: 20px;
  &:hover {
   background: rgba(100,100,100,0.6);
 }
`;

const SubItem = styled.div`
  color: white;
  font-size: 17px;
  margin-left: 40px;
  margin-bottom: 5px;
  &:hover {
   color: #33ccff;
 }
`;

class tileData extends React.Component{

  constructor(props){
    super(props);
  }

render(){
    return(
      <div>

    <Item>
      <i className="pi pi-th-large" style={{'fontSize': '28px'}}></i>Tournaments

        <Link to="/create/user" style={{ color: '#33ccff' }}><SubItem>Create User Tournament</SubItem></Link>
        <Link to="/create/team" style={{ color: '#33ccff' }}><SubItem>Create Team Tournament</SubItem></Link>
        <Link to="/create/bot" style={{ color: '#33ccff' }}><SubItem>Create Bot Tournament</SubItem></Link>

    </Item>

    <Item>
      <i className="pi pi-users" style={{'fontSize': '28px'}}></i>Participants

        <SubItem onClick={() => window.location = `${process.env.REACT_APP_USRMGMT}/#/usrmgmt/create`}>Create User</SubItem>
        <SubItem onClick={() => window.location = `${process.env.REACT_APP_USRMGMT}/#/usrmgmt/show`}>Manager Users</SubItem>
        <SubItem onClick={() => window.location = `${process.env.REACT_APP_USRMGMT}/#/usrmgmt/createteam`}>Create Team</SubItem>
        <SubItem onClick={() => window.location = `${process.env.REACT_APP_USRMGMT}/#/usrmgmt/showteams`}>Manage Teams</SubItem>
    </Item>

    <Item>
      <Link to="/show" style={{ color: '#33ccff' }}><i className="pi pi-globe" style={{'fontSize': '28px'}}></i>Games</Link>
    </Item>

    <Item>
      <Link to="/statistics" style={{ color: '#33ccff' }}><i className="pi pi-info-circle" style={{'fontSize': '28px'}}></i>Statistics</Link>
    </Item>

  </div>
)
}
}

export default tileData;
