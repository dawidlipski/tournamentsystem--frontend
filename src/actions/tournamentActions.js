import axios from 'axios';

export function fetchUsers(userList) {
  return {
    type: "FETCH_USERS",
    payload: axios.get(`${process.env.REACT_APP_USRMGMT}/user/all`)
  }
}

export function fetchTeams(team) {
  return {
    type: "FETCH_TEAMS",
    payload: axios.get(`${process.env.REACT_APP_USRMGMT}/team/all`)
  }
}

export function fetchUsersFromTeam(id) {
  return {
    type: "FETCH_USERS_FROM_TEAM",
    payload: axios.get(`${process.env.REACT_APP_USRMGMT}/team/${id}/users`)
  }
}

export function fetchBots(bot) {
  return {
    type: "FETCH_BOTS",
    payload: axios.get(`${process.env.REACT_APP_USRMGMT}/user/bots`)
  }
}

export function createTournament(tournament) {
  return {
    type: "CREATE_TOURNAMENT",
    payload: axios.post(`${process.env.REACT_APP_HOST}/tournament/create`, tournament)
  }
}

export function deleteTournament(id) {
  return {
    type: "DELETE_TOURNAMENT",
    payload: axios.delete(`${process.env.REACT_APP_HOST}/tournament/delete/${id}`)
  }
}

export function fetchTournaments(tournamentList) {
  return {
    type: "FETCH_TOURNAMENTS",
    payload: axios.get(`${process.env.REACT_APP_HOST}/tournament/all`)
  }
}


export function fetchTournamentById(id) {
  return {
    type: "FETCH_TOURNAMENT_BY_ID",
    payload: axios.get(`${process.env.REACT_APP_HOST}/tournament/get/${id}`)
  }
}

export function fetchMatch(id) {
  return {
    type: "FETCH_MATCH",
    payload: axios.get(`${process.env.REACT_APP_HOST}/game/get/${id}`)
  }
}

export function updateMatch(game) {
  return {
    type: "UPDATE_MATCH",
    payload: axios.post(`${process.env.REACT_APP_HOST}/game/finish`, game)
  }
}

export function createBotMatch(match) {
  return {
    type: "CREATE_MATCH",
    // payload: axios.post(`${process.env.REACT_APP_TG}`, match)
    payload: axios.post(`${process.env.REACT_APP_TG}`, match)
  }
}
